﻿using System;
using VirtuellesHaustier;

namespace Virtuelles_Haustier
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Willkommen, waehle deinen Tamagotchi.");
            Console.WriteLine("A) Tamagotchi");
            Console.WriteLine("B) Gudetama");
            Console.WriteLine("Ihre Wahl ");

            bool auswahlGueltig = false;

            VirtuellesHaustierKlasse tamagotchi = new Tamagotchi("Default");

            while (!auswahlGueltig)
            {
                char auswahl = Console.ReadKey().KeyChar;

                switch (auswahl)
                {
                    case 'A':
                    case 'a':
                        tamagotchi = new Tamagotchi(getName());
                        auswahlGueltig = true;
                        break;
                    case 'B':
                    case 'b':
                        tamagotchi = new Gudetama(getName());
                        auswahlGueltig = true;
                        break;
                    default:
                        Console.WriteLine("\nUngültige Eingabe");
                        break;
                }
            }

            bool isGameRunning = true;

            while(isGameRunning)
            {
                Console.WriteLine("\n\nName: " + tamagotchi.getName());
                Console.WriteLine("Alter (Runde): " + tamagotchi.getALter());
                Console.WriteLine("Hunger (Stufe 1-5): " + tamagotchi.getHunger());
                Console.WriteLine("Langeweile (Stufe 1-5): " + tamagotchi.getLangeweile());
                Console.WriteLine("Gesundheit (Stufe 1-5): " + tamagotchi.getGesundheit());
                Console.WriteLine("(f)uettern, (m)edizin, (s)pielen, (w)arten, oder (b)eenden");

                char auswahl2 = Console.ReadKey().KeyChar;

                switch(auswahl2)
                {
                    case 'f':
                        tamagotchi.Fuettern();
                        break;
                    case 'm':
                        tamagotchi.MedizinGeben();
                        break;
                    case 's':
                        tamagotchi.Spielen();
                        break;
                    case 'w':
                        tamagotchi.Warten();
                        break;
                    case 'b':
                        isGameRunning = false;
                        break;
                }

                if(!tamagotchi.isAlive())
                {
                    Console.WriteLine("\n\nDein Haustier ist leider gestorben.");
                    isGameRunning = false;
                }
            }

            Console.ReadLine();
        }

        static string getName()
        {
            Console.WriteLine("\n\nBitte den Namen deines Tamagotchi eingeben:" );
            string name = Console.ReadLine();

            return name;
        }
    }
}
